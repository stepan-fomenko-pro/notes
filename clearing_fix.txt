enum class Role
{
    Client = 0,
    Principal = 1,
    CoolPrincipalCommon = 2,
    CoolPrincipalWithOrderQuoter = 3,
    CoolPrincipalWithEcn = 4,
    Alfa = 5
};

enum class AccountType
{
    System = 0,
    Trader = 1,
    Counterparty = 2
};

Нерешенные проблемы:
Не пропадает тип Clearing из списка в фильтре таблицы на лету
+ Можно добавлять клиринг логин за менеджера без прав на редактирование
+ Можно видеть клиринг логины за менеджера без прав на просмотр
? Можно добавлять клиринг логин принципалу без клиринг фичи
+ Нельзя редактировать клиринг логин за менеджера с правами на редактирование
В AccessGroups видно поле Clearing
неправильные табы
Price2 в fix сообщениях
Валидация полей на GUI

Вопросы:
Может ли менеджер без прав на редактирование открывать/закрывать клиринг логины?

Тестирование:
+ 1. CRUD полей
+ 2. tab order
+ 3. Права на CRUD админ/менеджер
+ 4. Отображение изменений в аудите
+ 5. Save/Load в БД
+ 6. Очистка полей в форме добавления/изменения логина
+ 7. Видимость полей в форме добавления/изменения логинов.
+ 8. Валидация на непустые поля.
+ 9. view mode в таблице логинов
+ 10. отображение в таблице логинов
+ 11. Валидация на уникальность комбинации Moex FirmId/AccountId/UserId
+ 12. Проверить видимость полей клиринга в зависимости от принципальской фичи за менеджера и прав на видиние

Добавление клиринга в БД:
Под какие фирмы, акки создавать клиринговые логины?


Особенности:
При восстановлении, если незаполнен ExtOrderID, таймаут 5 секунд 
На OrderStatusRequest таймаут 5 мин
