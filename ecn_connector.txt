1. Инкременты или снапшоты?
2. Бенды или ордера?

Тестирование:
1. Маркетная часть
1.1 Проверить что приходят ордерные котировки
2.2 Проверить что можно подписаться на инкременты и на снапшоты (UseSubscribeIncrement в конфиге), дефолтный тип в setup-е
2.3 Проверить, что сессия стартует только после получения TradingSessionStatus
2. Ордерная часть
2.1 Проверить что подставляется правильный логин в зависимости от UseInitiatorLoginId
2.2 Проверить что все типы ордеров исполняются
2.3 Проверить что поддерживаются все времена жизни
2.4 Проверить Replace и Cancel для GTC
2.5 Проверить Reject на Replace и Cancel
2.6 Проверить Cancel по отключению Matching
2.7 Проверить Fill/Partial Fill после Replace
2.8 Проверить Partial Fill c последующим Fill/Cancel
2.9 Проверить Replace Reject с последующим Fill/Cancel
2.10 Проверить, что сессия стартует только после получения TradingSessionStatus
2.11 Проверить Cancel on Stop

limit IOC, GTC (без FullAmount)

Reject на Replace и Cancel
Cancel по отключению Matching
Пачка Replace c успешными replace + fill/part fill
partial fill разные кейсы
Replace Reject + Fill и другое
Стартуем сессию только после TradingSessionStatus + проверить у еще одного диалекта

Особенности:
- не поддерживается OrdStatus PartiallyFill (ConvertRules + ParserTests)
